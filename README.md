# IBM-assistant-reservation-otelo
Assistant virtuel de réservation grâce à IBM Watson relié à une application Nodejs, une base de données et Facebook Messenger.


https://frebourg.es/otelo-7/

[![Déploiement dans IBM Cloud](https://cloud.ibm.com/devops/setup/deploy/button.png)](https://cloud.ibm.com/devops/setup/deploy?repository=https://gitlab.com/cherryclass/otelo&branch=master)


Mise à jour:
08-20
adapation pour otelo
02-20   
Cognos Analytics   
NodeJs sur AWS et en local   
Assistant V2   
Amélioration de l'interface web   
03-19   
MariaDB sur AWS  
Suppression de la région à la connexion IBM Cloud   
Ajout d'une interface web   
12-18   
Migration vers cloud.ibm.com   
10-18   
Suppression de Watson Analytics   
