
DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'standard'),
(2, 'confort'),
(3, 'premium'),
(4, 'luxe');

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

DROP TABLE IF EXISTS `chambre`;
CREATE TABLE IF NOT EXISTS `chambre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nbCouchage` int(11) NOT NULL,
  `porte` varchar(5) NOT NULL,
  `etage` varchar(5) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `baignoire` tinyint(1) NOT NULL,
  `prixBase` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;



-- --------------------------------------------------------




-- --------------------------------------------------------

--
-- Structure de la table `periode`
--

DROP TABLE IF EXISTS `periode`;
CREATE TABLE IF NOT EXISTS `periode` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `periode`
--

INSERT INTO `periode` (`id`, `libelle`, `coefficient`) VALUES
(1, 'basse', 0.7),
(2, 'moyenne', 1),
(3, 'haute', 2);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

--DROP TABLE IF EXISTS `reservation`;
--CREATE TABLE IF NOT EXISTS `reservation` (
---  `id` int(11) NOT NULL AUTO_INCREMENT,
 -- `dateD` date NOT NULL,
--  `dateF` date NOT NULL,
--  `idPeriode` int(11) NOT NULL,
--  PRIMARY KEY (`id`),
 -- KEY `idPeriode` (`idPeriode`)
--) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Structure de la table `ligne_reservation`
--


DROP TABLE IF EXISTS `ligne_reservation`;
CREATE TABLE IF NOT EXISTS `ligne_reservation` (
  `idchambre` int(11) NOT NULL,
  `idreservation` int(11) NOT NULL,
  KEY `idchambre` (`idchambre`),
  KEY `idreservation` (`idreservation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Contraintes pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD CONSTRAINT `chambre_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `ligne_reservation`
--
--ALTER TABLE `ligne_reservation`
--  ADD CONSTRAINT `ligne_reservation_ibfk_1` FOREIGN KEY (`idchambre`) REFERENCES `chambre` (`id`),
--  ADD CONSTRAINT `ligne_reservation_ibfk_2` FOREIGN KEY (`idreservation`) REFERENCES `reservation` (`id`);

--
-- Contraintes pour la table `reservation`
--
--ALTER TABLE `reservation`
--  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`idPeriode`) REFERENCES `periode` (`id`);
COMMIT;

